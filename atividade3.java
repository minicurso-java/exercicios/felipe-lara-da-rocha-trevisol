import java.util.Scanner;
public class Lara {

	public static void main(String[] args) {
		Scanner obj = new Scanner(System.in);
		String nome;
		int horas;
		double salario, salarioHora;
		System.out.println("qual seu nome?");
			nome = obj.nextLine();
		System.out.println("digite o numero de horas trabalhadas e, em seguida, quanto recebe por hora");
			horas = obj.nextInt();
			salarioHora = obj.nextDouble();
		salario = (horas*salarioHora);
		System.out.printf("Bom dia, Sr(a). %s, seu salario é de: %.2f", nome, salario);
		
		obj.close();
	}

}
