import java.util.Dictionary;
import java.util.Hashtable;
import javax.swing.JOptionPane;

public class Projetitos {
  public static void main(String[] args){
      String a, b, c;
      double soma, subt, mult, div;
      a = JOptionPane.showInputDialog("digite o primeiro numero: ");
      double valor1 = Double.parseDouble(a);
      b = JOptionPane.showInputDialog("digite o segundo numero: ");
      double valor2 = Double.parseDouble(b);
      operadores(valor1, valor2);
    }

  public static void operadores(double num1, double num2){

      Dictionary dict = new Hashtable();
      dict.put("Soma", 1);
      dict.put("Subtração", 2);
      dict.put("Multiplicação", 3);
      dict.put("Divisão", 4);
      
      Object[] itens = { "Soma", "Subtração", "Multiplicação", "Divisão" };
      Object selectedValue = JOptionPane.showInputDialog(null, "Escolha um item", "Opçao", JOptionPane.INFORMATION_MESSAGE, null, itens, itens[2]);
      
      int opcao = (int) dict.get(selectedValue);
      
      if(opcao == 1){
        JOptionPane.showMessageDialog(null, "A soma é: "+ (num1 + num2));
      }
      else if(opcao == 2){
        JOptionPane.showMessageDialog(null, "A subtração é: "+ (num1 - num2));
      }
      else if(opcao == 3){
        JOptionPane.showMessageDialog(null, "a multiplicação é: "+  (num1 * num2));
      }
      else if(opcao == 4){
        JOptionPane.showMessageDialog(null, "a divisão é: "+ (num1/num2));
      }
      }
    
  }

