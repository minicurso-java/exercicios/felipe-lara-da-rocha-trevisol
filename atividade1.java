import java.util.Scanner;
import java.util.Locale;

public class atividade1{
  public static void main(String[] args){
    Locale.setDefault(Locale.US);
    Scanner myObj = new Scanner(System.in);
    System.out.println("Entre com o valor de A (US pattern): ");
      double A = myObj.nextDouble();
    System.out.println("Entre com o valor de B (US pattern):  ");
      double B = myObj.nextDouble();
    System.out.println("Entre com o valor de C (US pattern): ");
      double C = myObj.nextDouble();
        double areaTrianguloRet = (A*C)/2;
        double areaCirculo = (3.14159*(C*C));
        double areaTrapezio = (((A+B)/2)*C);
        double areaQuadrado = (B*B);
        double areaRetangulo = (A*B);
    System.out.printf("triangulo retangulo: %.3f\n", areaTrianguloRet);
    System.out.printf("circulo: %.3f\n", areaCirculo);
    System.out.printf("trapezio: %.3f\n", areaTrapezio);
    System.out.printf("quadrado: %.3f\n", areaQuadrado);
    System.out.printf("retangulo: %.3f\n", areaRetangulo);
}
}
